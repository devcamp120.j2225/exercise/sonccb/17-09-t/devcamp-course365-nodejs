//Import thư viện expressjs tương đương import express from "express"; 
const express = require("express");

//Import thư viện path
const path = require("path");

//import thư viện mongoose
const mongoose = require("mongoose");

//import schema
const courseSchema = require("./app/models/courseModel");

//Import router
const { courseRouter } = require("./app/routes/courseRouter");

//Khởi tạo 1 app express 
const app = express();

//Khai báo cổng chạy project
const port = 8000;

//cấu hình API để sử dụng được body json
app.use(express.json());

//cấu hình API để sử dụng body unicode để đọc tiếng Việt
app.use(express.urlencoded({
    extended:true
}));

//sử dụng static() để render các nội dung tĩnh (như ảnh, file css)
app.use(express.static(__dirname + "/views"));

//kết nối với mongoDB
mongoose.connect("mongodb://localhost:27017/CRUD_Course365", function (error){
    if (error) throw error;
    console.log("Successfully connected to MongoDB");
});



//***Main code starts here
//API, GET method
app.get("/", (req, res) => {
    console.log(__dirname);

    res.sendFile(path.join(__dirname + "/views/index.html"));
});

app.get("/allcourses", (req, res) => {
    console.log(__dirname);

    res.sendFile(path.join(__dirname + "/views/allCourses.html"));
});

//sử dụng router
app.use("/", courseRouter)

//app listen on port
app.listen(port, () => {
    console.log("App listening on port: ", port);
});