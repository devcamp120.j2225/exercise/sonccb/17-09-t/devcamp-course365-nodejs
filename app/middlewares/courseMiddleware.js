const courseMiddleware = (req, res, next) => {
    console.log(`Method: ${req.method} - URL: ${req.url} - Time: ${new Date()}`);

    next();
};

//export course middleware thành 1 module
module.exports = { courseMiddleware }; 