//import thư viện mongoose
const mongoose = require("mongoose");

//import course model
const courseModel = require("../models/courseModel");

//CRUD
//C - Create course list - POST method
const createCourse = (req, res) => {
    //B1: Thu thập dữ liệu từ req body
    let body = req.body;

    //B2: Validate dữ liệu
    //courseCode
    if(!body.courseCode) {
        return res.status(400).json({
            message: `Course Code is required!`
        });
    };

    //courseName
    if(!body.courseName) {
        return res.status(400).json({
            message: `Course Name is required`
        });
    };

    //price
    if(!body.price) {
        return res.status(400).json({
            message: `Price is required`
        });
    };

    if(!Number.isInteger(body.price) || body.price < 0) {
        return res.status(400).json({
            message: `Price is invalid`
        });
    };
    //discountPrice
    if(!body.discountPrice) {
        return res.status(400).json({
            message: `Discount Price is required`
        });
    };

    if(!Number.isInteger(body.discountPrice) || body.discountPrice < 0) {
        return res.status(400).json({
            message: `Discount Price is invalid`
        });
    };
    //duration
    if(!body.duration) {
        return res.status(400).json({
            message: `Duration is required`
        });
    };
    //level
    if(!body.level) {
        return res.status(400).json({
            message: `Level is required`
        });
    };
    //coverImage
    if(!body.coverImage) {
            return res.status(400).json({
                message: `Cover Image is required`
            });
    };
    //teacherName
    if(!body.teacherName) {
        return res.status(400).json({
            message: `Teacher Name is required`
        });
    };
    //teacherPhoto
    if(!body.teacherPhoto) {
        return res.status(400).json({
            message: `Teacher Photo is required`
        });
    };
    // //isPopular
    if(!body.isPopular == "boolean") {
        return res.status(400).json({
            message: `Is Popular is required a value of true or false`
        });
    };
    // //isTrending
    if(!body.isTrending == "boolean") {
        return res.status(400).json({
            message: `Is Popular is required a value of true or false`
        });
    };

    //B3: Gọi model xử lý dữ liệu
    let newCourse = {
        _id: mongoose.Types.ObjectId(),
        courseCode: body.courseCode,
        courseName: body.courseName,
        price: body.price,
        discountPrice: body.discountPrice,
        duration: body.duration,
        level: body.level,
        coverImage: body.coverImage,
        teacherName: body.teacherName,
        teacherPhoto: body.teacherPhoto,
        isPopular: body.isPopular,
        isTrending: body.isTrending
    };

    //sử dụng function create() để tạo data
    courseModel.create(newCourse, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            });
        };
        return res.status(201).json({
            messsage: "Create successfully",
            newCourse: data
        });
    });
};

//R - Get all courses - GET method
const getAllCourses = (req, res) => {
    //B1: Thu thập dữ liệu từ req
    //B2: Validate dữ liệu
    //B3: Gọi model xử lý dữ liệu
    courseModel.find((error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            });
        };

        return res.status(200).json({
            message: `Get all courses successfully`,
            courses: data
        });
    });
};

//R - Get one course by courseId - GET method
const getCourseById = (req, res) => {
    //B1: Thu thập dữ liệu từ req
    let id = req.params.courseId;
    //B2: Validate dữ liệu
    //id
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: `Course ID: ${id} is invalid`
        });
    };
    //B3: Gọi model xử lý dữ liệu
    courseModel.findById(id, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            });
        };

        return res.status(200).json({
            message: `Get course successfully. Course ID: ${id}`,
            courses: data
        });
    });
};

//U - Update one course by courseId - PUT method
const updateCourseById = (req, res) => {
    //B1: Thu thập dữ liệu từ req
    let id = req.params.courseId;
    let body = req.body;

    //B2: Validate dữ liệu
    //id
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: `Course ID: ${id} is invalid`
        });
    };

    //body
    if(!body.courseCode && body.courseCode !== undefined) {
        return res.status(400).json({
            message: `Course Code is required!`
        });
    };

    //courseName
    if(!body.courseName && body.courseName !== undefined) {
        return res.status(400).json({
            message: `Course Name is required`
        });
    };

    //price
    if(!body.price && body.price !== undefined) {
        return res.status(400).json({
            message: `Price is required`
        });
    };

    if(!Number.isInteger(body.price) || body.price < 0) {
        return res.status(400).json({
            message: `Price is invalid`
        });
    };
    //discountPrice
    if(!body.discountPrice && body.discountPrice !== undefined) {
        return res.status(400).json({
            message: `Discount Price is required`
        });
    };

    if(!Number.isInteger(body.discountPrice) || body.discountPrice < 0) {
        return res.status(400).json({
            message: `Discount Price is invalid`
        });
    };
    //duration
    if(!body.duration && body.duration !== undefined) {
        return res.status(400).json({
            message: `Duration is required`
        });
    };
    //level
    if(!body.level && body.level !== undefined) {
        return res.status(400).json({
            message: `Level is required`
        });
    };
    //coverImage
    if(!body.coverImage && body.coverImage !== undefined) {
            return res.status(400).json({
                message: `Cover Image is required`
            });
    };
    //teacherName
    if(!body.teacherName && body.teacherName !== undefined) {
        return res.status(400).json({
            message: `Teacher Name is required`
        });
    };
    //teacherPhoto
    if(!body.teacherPhoto && body.teacherPhoto !== undefined) {
        return res.status(400).json({
            message: `Teacher Photo is required`
        });
    };

    //B3: Gọi model để xử lý dữ liệu
    let courseUpdate = {
        courseCode: body.courseCode,
        courseName: body.courseName,
        price: body.price,
        discountPrice: body.discountPrice,
        duration: body.duration,
        level: body.level,
        coverImage: body.coverImage,
        teacherName: body.teacherName,
        teacherPhoto: body.teacherPhoto,
        isPopular: body.isPopular,
        isTrending: body.isTrending
    };

    courseModel.findByIdAndUpdate(id, courseUpdate, (error, data) => {
        if(error) {
            res.status(500).json({
                message: error.message
            });
        };

        return res.status(200).json({
            message: `Update course successfully. Course ID: ${id}`,
            courseUpdate: data
        });
    });
};


//D - Delete one course by courseId - DELETE method
const deleteCourseById = (req, res) => {
    //B1: Thu thập dữ liệu
    let id = req.params.courseId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: `Course ID is invalid`
        });
    };

    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    courseModel.findByIdAndDelete(id, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            });
        };

        return res.status(204).json({
            message: `Delete course successfully. Course ID: ${id}`
        });
    });
};


//export course controller thành 1 module
module.exports = {
    createCourse,
    getAllCourses,
    getCourseById,
    updateCourseById,
    deleteCourseById
};
