//Khai báo thư viện express
const express = require("express");

//Import drinkMiddleware
const { courseMiddleware } = require("../middlewares/courseMiddleware");

//Import course controller
const { createCourse, 
        getAllCourses, 
        getCourseById, 
        updateCourseById, 
        deleteCourseById } = require("../controllers/courseController");

//Khởi tạo Router
const courseRouter = express.Router();

//sử dụng courseMiddleware
courseRouter.use(courseMiddleware);


//C - Create, POST method, create one course
courseRouter.post("/courses", createCourse);

//R - Read, GET method, get all courses
courseRouter.get("/courses", getAllCourses);

//R - Read, GET method, get one course by courseId
courseRouter.get("/courses/:courseId", getCourseById);

//U - Update, PUT method, update one course by courseId
courseRouter.put("/courses/:courseId", updateCourseById);

//D - Delete, DELETE method, delete one course by courseId
courseRouter.delete("/courses/:courseId", deleteCourseById);

//export course router thành 1 module
module.exports = { courseRouter };