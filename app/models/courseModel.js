//import thư viện mongoose
const mongoose = require("mongoose");
// khai báo Schema
const Schema = mongoose.Schema;

//khai báo mođel course
const courseSchema = new Schema({
    _id: {
        type: Schema.Types.ObjectId
    },
    courseCode: {
        type: String,
        unique: true,
        required: true
    },
    courseName: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    discountPrice: {
        type: Number,
        required: true
    },
    duration: {
        type: String,
        required: true
    },
    level: {
        type: String,
        required: true
    },
    coverImage: {
        type: String,
        required: true
    },
    teacherName: {
        type: String,
        required: true
    },
    teacherPhoto: {
        type: String,
        required: true
    },
    isPopular: {
        type: Boolean,
        default: true
    },
    isTrending: {
        type: Boolean,
        default: false  
    }
}, {
    timestamps: true
});

//export
module.exports = mongoose.model("course", courseSchema);