$(document).ready(function () {
    onPageLoading();

});
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 14,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        }
    ]
};







/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
// Gán sự kiện cho nút browse courses để link sang trang all courses
$("#btn-browse-courses").on("click", function () {
    onBtnBrowseCourses();
});







/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */  
function onPageLoading () {
    console.log("Test hàm onload");
    // Hàm hiển thị popular courses
    displayPopularCourses();
    displayTrendingCourses();
};








/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// Khai báo hàm hiển thị popular courses
function displayPopularCourses () {
    var vPopularCardDeck = $(".popular-card");
    var vTempHtml = ""; 
    vPopularCardDeck.empty();

    for (var bI = 0; bI < gCoursesDB.courses.length; bI++) {
        var bResult = gCoursesDB.courses[bI];
        console.log(bResult);

        if (bResult.isPopular) {
            vTempHtml += `<div class="card">
                <!--  card header img -->
                <img class="card-img-top" src= `+ bResult.coverImage +` alt="img-card-header" />
                <!-- card body -->
                <div class="card-body">
                <div class="row m-0">
                    <h5 class="card-title text-primary"> `+ bResult.courseName +` </h5>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                    <i class="far fa-clock"></i>
                    </div>  
                    <div class="col-sm-10 text-left p-0">
                    <span> `+ bResult.duration +`</span>
                    <span>&nbsp; `+ bResult.level +`</span>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-sm-12">
                    <span class = "font-weight-bold">$ `+ bResult.discountPrice +`</span> 
                    <span><del>$ `+ bResult.price +`</del></span>
                    </div>
                </div>
                </div>
                <!-- card footer -->
                <div class="card-footer">
                <div class="row">
                    <div class="col-sm-2">
                    <img class = "rounded-circle img-footer" src= `+ bResult.teacherPhoto +` alt="img-footer" height = "30" width = "30" />
                    </div>
                    <div class="col-sm-8">
                    <small class = "text-muted"> `+ bResult.teacherName +` </small>
                    </div>
                    <div class="col-sm-2">
                    <i class="far fa-bookmark"></i>
                    </div>
                </div>
                </div>
            </div>`
        }
    };

    vPopularCardDeck.html(vTempHtml);
};

function displayTrendingCourses () {
    var vTrendingCardDeck = $(".trending-card");
    var vTempHtml = ""; 
    vTrendingCardDeck.empty();

    for (var bI = 0; bI < gCoursesDB.courses.length; bI++) {
        var bResult = gCoursesDB.courses[bI];
        console.log(bResult);

        if (bResult.isTrending) {
            vTempHtml += `<div class="card">
                <!--  card header img -->
                <img class="card-img-top" src= `+ bResult.coverImage +` alt="img-card-header" />
                <!-- card body -->
                <div class="card-body">
                <div class="row m-0">
                    <h5 class="card-title text-primary"> `+ bResult.courseName +` </h5>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                    <i class="far fa-clock"></i>
                    </div>  
                    <div class="col-sm-10 text-left p-0">
                    <span> `+ bResult.duration +`</span>
                    <span>&nbsp; `+ bResult.level +`</span>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-sm-12">
                    <span class = "font-weight-bold">$ `+ bResult.discountPrice +`</span> 
                    <span><del>$ `+ bResult.price +`</del></span>
                    </div>
                </div>
                </div>
                <!-- card footer -->
                <div class="card-footer">
                <div class="row">
                    <div class="col-sm-2">
                    <img class = "rounded-circle img-footer" src= `+ bResult.teacherPhoto +` alt="img-footer" height = "30" width = "30" />
                    </div>
                    <div class="col-sm-8">
                    <small class = "text-muted"> `+ bResult.teacherName +` </small>
                    </div>
                    <div class="col-sm-2">
                    <i class="far fa-bookmark"></i>
                    </div>
                </div>
                </div>
            </div>`
        }
    };

    vTrendingCardDeck.html(vTempHtml);
};

// Khai báo nút browse courses
function onBtnBrowseCourses() {
    console.log("Nút browse courses được click");
    window.location.href = "allCourses.html";
};




